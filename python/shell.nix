let
    basepkgs = import <nixpkgs> {};

    pkgs = import (basepkgs.fetchFromGitHub {
        owner = "nixos";
        repo = "nixpkgs";
        rev = "e4af779056318fa830de52080dca08464d0d2bf9";
        sha256 = "0lnqr9lfavn89n66k48p8bcvcv0i2i84izwr5918qvq57dwzyjqm";
    }) {};

    src = pkgs.lib.cleanSource ./.;
    pyproject = ./pyproject.toml;
    poetrylock = ./poetry.lock;
    python = pkgs.python3;

in
    # pkgs.poetry2nix.mkPoetryEnv {
    #     inherit poetrylock python;
    # }
    pkgs.poetry2nix.mkPoetryApplication {
        inherit poetrylock python src pyproject;
    }
